var AWS = require("aws-sdk");

exports.handler = (event, context, callback) => {
    var route53 = new AWS.Route53();
    var params = {
        ChangeBatch: {
            Changes: [{
                Action: "UPSERT",
                ResourceRecordSet: {
                    Name: process.env.RECORD_NAME,
                    ResourceRecords: [{
                        Value: event.requestContext.identity.sourceIp
                    }],
                    TTL: 300,
                    Type: "A"
                }
            }],
            Comment: "Web server for example.com"
        },
        HostedZoneId: process.env.HOSTED_ZONE_ID
    };
    route53.changeResourceRecordSets(params, function(err, data) {
        if (err) {
            console.log(err, err.stack); // an error occurred
            callback(null, {
                statusCode: 500,
                body: JSON.stringify(err)
            });
        }
        else {
            console.log(data); // successful response
            callback(null, {
                statusCode: 200,
                body: JSON.stringify(data)
            });
        }
    });
};
