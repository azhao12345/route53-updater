# Route53 Updater

Minimal lambda function to update dns records for dynamic ip.  The AWS sample solution is too complicated, and other solutions all require third party integrations


## Setup
1. (set up lambda and api gateway)[https://docs.aws.amazon.com/apigateway/latest/developerguide/api-gateway-create-api-as-simple-proxy-for-lambda.html]
2. set environment variables `HOSTED_ZONE_ID` and `RECORD_NAME`
3. set up api keys if needed


## Usage
```
curl -H "x-api-key: my-api-key" -v https://my-endpoing.execute-api.us-east-1.amazonaws.com/my-stage/my-api
```
